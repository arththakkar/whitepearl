class CompaniesController < ApplicationController
	before_action :set_company, only: [:edit, :update, :show, :destroy]

	def index
		@companies = Company.all
	end

	def new
		@company = Company.new
	end

	def create
		@company = Company.new(company_params)
		if @company.save
			flash.now[:success] = "Company created successfully."
			redirect_to company_path(@company)
		else
			flash.now[:danger] = @company.errors.full_messages
			render :new
		end
	end

	def edit
	end

	def update
	end

	def destroy
	end

	def show
	end

	private
	def company_params
		params.require(:company).permit(:name, :address, :contact_person_name, :contact_number, :second_contact_person_name, :second_contact_number, :contract_start_date, :contract_end_date, :authorized_person_name)
	end

	def set_company
	end
end