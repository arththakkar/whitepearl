class UsersController < ApplicationController
	before_filter :set_user, only: [:edit, :update, :destroy, :show]
	def index
  	@users = User.all
  end

  def new
  	@user = User.new
  end

  def create
  	raise "innn"
  	@user = User.new(user_params)
  	raise @user.inspect
  	@user.password = "12345678"
  	puts "=================#{@user.inspect}"
  	if @user.save
  		redirect_to users_path, notice: "User created successfully."
  	else
  		redirect_to new_user_path, alert: @user.errors.full_messages
  	end
  end

  def edit
  end

  def update
  	if @user.update_attributes(user_params)
  		redirect_to users_path, notice: "User updated successfully."
  	else
  		redirect_to edit_user_path, alert: @user.errors.full_messages
  	end
  end

  def show
  end

  def destroy
  	@user.destroy
  	redirect_to users_path, notice: "User deleted successfully."
  end

  private
  def user_params
  	params.require(:user).permit(:email, :first_name, :last_name, :mobile_number, :employee_type)
  end

  def set_user
  	@user = User.find(params[:id])
  end
end
