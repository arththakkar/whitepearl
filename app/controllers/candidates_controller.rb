class CandidatesController < ApplicationController
	respond_to :docx
	def index
		@candidates = Candidate.all
	end

	def new
		@candidate = Candidate.new
	end

	def create
		@candidate = current_user.candidates.build(candidate_params)
		if @candidate.save
			redirect_to candidates_path, notice: "Candidate created successfully."
		else
			redirect_to new_candidate_path, notice: "Something went wrong."
		end
	end

	def edit
		@candidate = Candidate.find(params[:id])
	end

	def update
		@candidate = Candidate.find(params[:id])
		if @candidate.update_attributes(candidate_params)
			redirect_to candidates_path, notice: "Candidate updated successfully."
		else
			redirect_to edit_candidate_path(@candidate), notice: "Something went wrong."
		end
	end

	def destroy
	end

	def show
	end

	def download_front
		@candidate = Candidate.find(params[:id])
		respond_to do |format|
			format.docx do
				render docx: 'download_front', filename: "#{@candidate.wpc_designation}_#{@candidate.first_name}.docx"
			end
		end
	end

	private
	def candidate_params
		params.require(:candidate).permit(:entered_date, :first_name, :last_name, :contact_number, :email, :wpc_designation, :company_id, :date_of_birth, :qualification, :experience, :current_company, :current_designation, :current_location, :open_for_location, :current_ctc, :expected_ctc, :notice_period, :reason_for_change, :reffered_for, :resume)
	end

end