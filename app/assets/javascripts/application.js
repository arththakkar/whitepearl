// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui.min
// require turbolinks
//= require bootstrap-sprockets
//= require raphael-min
//= require plugins/morris/morris.min
//= require plugins/sparkline/jquery.sparkline.min
//= require plugins/jvectormap/jquery-jvectormap-1.2.2.min
//= require plugins/jvectormap/jquery-jvectormap-world-mill-en
//= require plugins/knob/jquery.knob
//= require moment.min
//= require plugins/daterangepicker/daterangepicker
//= require plugins/datepicker/bootstrap-datepicker
//= require plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min
//= require plugins/slimScroll/jquery.slimscroll.min
//= require plugins/fastclick/fastclick
//= require plugins/select2/select2.full.min
//= require dist/js/app.min
//= require dist/js/pages/dashboard
//= require dist/js/demo
// require_tree .
