class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Associations
  has_many :candidates

  # Instance Methods
  def fullname
  	"#{first_name} #{last_name}"
  end

  def is_admin?
  	return ( employee_type == "Admin" ? true : false )
  end

  def is_employee?
  	return ( employee_type == "Employee" ? true : false )
  end
end
