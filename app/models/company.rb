class Company < ActiveRecord::Base

	# Associations
	has_many :candidates

	# Validations
	validates :name, :address, :contact_number, :contact_person_name, :contract_start_date, :contract_end_date, :authorized_person_name, presence: true
	validate :contact_start_date_lesser_than_contract_end_date

	# Validation Methods
	def contact_start_date_lesser_than_contract_end_date
		if contract_start_date < contract_end_date
			errors.add(:contract_end_date, "can't be before Contract Start Date")
		end
	end
end
