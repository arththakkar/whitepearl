class Candidate < ActiveRecord::Base

	# Associations
  belongs_to :user
  belongs_to :company

  has_attached_file :resume
  validates_attachment_content_type :resume, content_type: ["application/msword"]

  # Instance Methods
  def fullname
  	"#{first_name} #{last_name}"
  end
end
