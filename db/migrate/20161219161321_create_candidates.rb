class CreateCandidates < ActiveRecord::Migration
  def change
    create_table :candidates do |t|
      t.string :resume_number
      t.date :entered_date
      t.string :first_name
      t.string :last_name
      t.string :contact_number
      t.string :email
      t.string :wpc_designation
      t.string :wpc_company
      t.date :date_of_birth
      t.string :qualification
      t.string :experience
      t.string :current_company
      t.string :current_designation
      t.string :current_location
      t.string :open_for_location
      t.string :current_ctc
      t.string :expected_ctc
      t.string :notice_period
      t.string :reason_for_change
      t.text :reffered_for
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
