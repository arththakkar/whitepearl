class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.text :address
      t.string :contact_number
      t.string :contact_person_name
      t.string :second_contact_number
      t.string :second_contact_person_name
      t.string :contract_start_date
      t.string :contract_end_date
      t.string :authorized_person_name

      t.timestamps null: false
    end
  end
end
