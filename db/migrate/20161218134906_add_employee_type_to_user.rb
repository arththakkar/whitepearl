class AddEmployeeTypeToUser < ActiveRecord::Migration
  def change
    add_column :users, :employee_type, :string
  end
end
