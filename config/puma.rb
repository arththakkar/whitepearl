#!/usr/bin/env puma

environment ENV['RAILS_ENV'] || 'production'

daemonize true

pidfile "/home/white_pearl/shared/tmp/pids/puma.pid"
stdout_redirect "/home/white_pearl/shared/tmp/log/stdout", "/home/white_pearl/shared/tmp/log/stderr"

threads 0, 16

bind "unix:///home/white_pearl/shared/tmp/sockets/puma.sock"