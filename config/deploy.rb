require 'mina/rails'
require 'mina/git'
# require 'mina/rbenv'  # for rbenv support. (https://rbenv.org)
require 'mina/rvm'    # for rvm support. (https://rvm.io)
require 'mina/puma'
# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)

set :application_name, 'foobar'
set :domain, 'gryfalcon.com'
set :deploy_to, '/home/white_pearl'
set :app_path, lambda { "#{deploy_to}/#{current_path}" }
set :repository, 'git@bitbucket.org:arththakkar/whitepearl.git'
set :branch, 'master'
set :rvm_use_path, '/usr/local/rvm/scripts/rvm'
# Optional settings:
set :user, 'root'          # Username in the server to SSH to.
# set :port, '5000'           # SSH port number.
#   set :forward_agent, true     # SSH forward_agent.

# shared dirs and files will be symlinked into the app-folder by the 'deploy:link_shared_paths' step.
# set :shared_dirs, fetch(:shared_dirs, []).push('somedir')
# set :shared_files, fetch(:shared_files, []).push('config/database.yml', 'config/secrets.yml')
set :shared_paths, ['config/database.yml', 'tmp/pids', 'tmp/sockets', 'log']

# This task is the environment that is loaded for all remote run commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  # If you're using rbenv, use this to load the rbenv environment.
  # Be sure to commit your .ruby-version or .rbenv-version to your repository.
  # invoke :'rbenv:load'

  # For those using RVM, use this to load an RVM version@gemset.
  invoke :'rvm:use', 'ruby-2.2.1@white_pearl'
end

# Put any custom commands you need to run at setup
# All paths in `shared_dirs` and `shared_paths` will be created on their own.
task :setup do
  # command %{rbenv install 2.3.0}
  # command %{rake db:create}
end

desc "Deploys the current version to the server."
task :deploy do
  # uncomment this line to make sure you pushed your local branch to the remote origin
  # invoke :'git:ensure_pushed'
  deploy do
    # Put things that will set up an empty directory into a fully set-up
    # instance of your project.
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_create'
    invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'
    invoke :'deploy:cleanup'

    on :launch do
      in_path(fetch(:current_path)) do
        command %{mkdir -p tmp/}
        command %{touch tmp/restart.txt}
      end
    end
  end

  # you can use `run :local` to run tasks on local machine before of after the deploy scripts
  # run(:local){ say 'done' }
end
namespace :puma do
  desc "Start the application"
  task :start do
    command %{echo "-----> Start Puma"}
    command %{cd /home/white_pearl/current && RAILS_ENV=production && bin/puma.sh start}
  end

  desc "Stop the application"
  task :stop do
    command %{echo "-----> Stop Puma"}
    command %{cd /home/white_pearl/current && RAILS_ENV=production && bin/puma.sh stop"}
  end

  desc "Restart the application"
  task :restart do
    command %{echo "-----> Restart Puma"}
    command %{cd /home/white_pearl/current && RAILS_ENV=production && bin/puma.sh restart}
  end
end

# For help in making your deploy script, see the Mina documentation:
#
#  - https://github.com/mina-deploy/mina/tree/master/docs
